#include <stdio.h>
#define SIZE 10
void push(int);
void pop();
void display();
int isEmpty();
int isFull();
void checkFull();
void checkEmpty();
int stack[SIZE], top=-1;
void main()
{
    int value,choice;
    while(1)
    {
        printf("\n\n***MENU***\n");
        printf("1. push\n2. pop\n3. Display\n4. Check Empty\n5. Check Full\n6. Exit");
        printf("\n Enter your choice: ");
        scanf("%d",&choice);
        switch(choice)
        {
            case 1: printf("Enter the value to insert: ");
                    scanf("%d",&value);
                    push(value);
                    break;
            case 2:
                pop();
                break;
            case 3:
                display();
                break;
            case 4:
                checkEmpty();
                break;
            case 5:
                checkFull();
                break;
            case 6: exit(0);
            default: printf("\n Wrong selection!!! try again");
        }
    }
}

void push(int value)
{
    if(isFull())
        printf("Insertion is not possible\n");
    else {
        top++;
        stack[top]= value;
    }
}

void pop()
{
    int popData;
    if(isEmpty())
        printf("Extraction is not possible\n");
    else {
        popData = stack[top];
        stack[top]=0;
        top-- ;
        printf("%d has removed !\n", popData);
    }
}

void display()
{
    if(!isEmpty()){
        printf("Stack elements are \n" );
        int i;
        for( i=0 ; i <=top ; i++)
        printf("%d\n", stack[i]);
    }

}

int isFull()
{
    if(top==SIZE-1){
        printf("\n Stack is Full!!!");
        return 1;
    }
    return 0;
}

int isEmpty()
{
    if(top==-1){
        printf("\n Stack is empty!!!");
        return 1;
    }
    return 0;
}

void checkEmpty()
{
    if(!isEmpty())
        printf("\nStack is not Empty.");
}

void checkFull()
{
    if(!isFull())
        printf("\nStack is not full");
}
