#include <stdio.h>
int main()
{
    int n, i;
    float sum = 0.0;
    float avg;

    printf("Enter a number : ");
    scanf("%d", &n);

    int A[n];
    printf("\nEnter %d number to calculate average of them : ", n);

    for(i = 0; i < n; i++){
        scanf("%d", &A[i]);
        sum += A[i];
    }
    avg = sum / n;

    printf("\nThe average is : %.2f\n", avg);

    return 0;
}
