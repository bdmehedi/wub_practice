#include <stdio.h>

int main()
{
    int bsize[10], psize[10], bno, pno, flags[10], allocation[10], i, j, temp, bigBlock[10], k;

    for(i = 0; i < 10; i++){
        flags[i] = 0;
        allocation[i] = -1;
    }

    printf("Enter no. of blocks: ");
    scanf("%d", &bno);

    printf("\nEnter size of each block: \n");
    for(i = 0; i < bno; i++){
        scanf("%d", &bsize[i]);
    }

    printf("\nEnter no. of processes: ");
    scanf("%d", &pno);

    printf("\nEnter size of each process: ");
    for(i = 0; i < pno; i++){
        scanf("%d", &psize[i]);
    }



    for(i = 0; i < pno; i++){
        k = 0;
        bigBlock[k] = -1;
        for(j = 0; j < bno; j++){
            if(flags[j] == 0 && bsize[j] >= psize[i]){
                bigBlock[k] = j;
                k++;
            }
        }

        if(bigBlock[0] != -1){
            for(int l = 0; l < k; l++){
                for(int m = l + 1; m < k; m++){
                    if(bsize[bigBlock[l]] > bsize[bigBlock[m]]){
                        temp = bigBlock[l];
                        bigBlock[l] = bigBlock[m];
                        bigBlock[m] = temp;
                    }
                }

            }

            flags[bigBlock[0]] = 1;
            allocation[bigBlock[0]] = i;
        }

    }

    printf("\nBlock no. \tsizse\tprocess no. \t\t\tsize");
    for(i = 0; i < bno; i++){
        printf("\n%d\t\t%d\t\t", i+1, bsize[i]);
        if(flags[i] == 1)
            printf("%d\t\t\t%d", allocation[i]+1, psize[allocation[i]]);
        else
            printf("Not allocated");
    }

    return 0;
}

