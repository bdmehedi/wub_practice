#include <stdio.h>
int main()
{
    int size, i, sElement;
    printf("Enter an integer number to define the list size : ");
    scanf("%d", &size);
    int lis[size];

    printf("\n Enter any %d integer value : ", size);
    for(i = 0; i < size; i++)
        scanf("%d", &lis[i]);


    printf("\n Enter any element to be search : ");
    scanf("%d", &sElement);

    for(i = 0; i < size; i++){
        if(sElement == lis[i]){
            printf("\n Element is found at %d index. \n", i);
            break;
        }
    }

    if(i == size)
        printf("Given value not found in the list !!! ");

    return 0;
}
