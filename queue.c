#include <stdio.h>
#define MAXSIZE 20

int isEmpty();
int isFull();
int enqueue(int element);
int dequeue();
int peek();
int front = -1, rear = -1, queue[MAXSIZE];

int main()
{
    int number, toInsertData;
    while(1){
        printf("##### Menu #####\n");
        printf("Please choose an option : \n");
        printf("#1. Insert\n#2. Delete\n#3. Peek\n#4.exit\n");
        scanf("%d", &number);

        switch(number){
            case 1:
                printf("Enter an Integer element to insert in queue : ");
                scanf("%d", &toInsertData);
                enqueue(toInsertData);
                break;
            case 2:
                deque();
                break;
            case 3:
                peek();
                break;
            case 4:
                exit(1);
            default :
                printf("\nWrong selection ! Try again");
        }
    }

    return 0;
}


int isEmpty()
{
    if(front < 0 || front > rear)
        return 1;

    return 0;
}

int isFull()
{
    if(rear == MAXSIZE - 1)
        return 1;

    return 0;
}

int enqueue(int data)
{
    if(isFull()){
        printf("\nQueue is Full ! insertion is not possible");
        return 0;
    }

    rear = rear + 1;
    queue[rear] = data;
    return 1;
}

int deque()
{
    if(isEmpty()){
        printf("\nQueue is empty ! Delete not possible .");
        return 0;
    }
    int data = queue[front];
    front = front + 1;
    return data;
}

int peek()
{
    if(isEmpty()){
        printf("\nQueue is empty !");
        return 0;
    }
    return queue[front];
}
